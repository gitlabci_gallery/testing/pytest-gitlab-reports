# Python test reports on Gitlab

This example show how to use Gitlab test reports and coverage feature on a Python project using pytest.

Gitlab version: 15.5 (when writing this example).

[[_TOC_]]

## Source code

The source code example reuses project configuration and some test examples described in [pytest documentation](https://docs.pytest.org/en/7.2.x/contents.html).

## Gitlab-CI configuration

Requirements:
* CI/CD is enabled: `Settings/General/Visibility, project features, permissions/Repository/'CI/CD'`
* Shared runners are enabled : `Settings/'CI/CD'/Runners/Shared runners/Enable shared runners for this project`

Ref:
* [Gitlab docs: test reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html)
* [Gitlab docs: test coverage](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html)

The gitlab-ci configuration is written in the file [.gitlab-ci.yml](.gitlab-ci.yml).

Gitlab may use test reports to provide some UI functionalities if you upload them as artifacts.

Supported formats:
* test report : junit (cf. [gitlab-docs](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit))
* coverage report : cobertura (cf. [gitlab-docs](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscoverage_report))

From [gitlab docs](https://docs.gitlab.com/ee/ci/yaml/#coverage), the coverage is shown in the UI if at least one line in the job output matches the regular expression defined with the `coverage` keyword.

In our `gitlab-ci.yml` file:
```
tests:
  ...
  coverage: '/^TOTAL.+?(\d+\%)$/'
```

You can download these artifacts on [Pipeline page](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/pipelines) with the download button on the right of a pipeline.

## Visualization on Gitlab UI

### Test reports

On [latest pipeline page](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/pipelines/main/latest), the Tests tab displays a list of test suites and cases reported from the XML file.

### Coverage

#### In Merge Requests

Two MRs have been created to provide visualization examples:
* [1](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/merge_requests/1) : test failure
* [2](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/merge_requests/2) : coverage on diff

#### In Analytics

You can view a graph of coverage evolution on default branch in `Analytics` menu.

For example, on this project, it is visible at:  https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/graphs/main/charts.

#### Adding coverage badge

To add a coverage badge to your project, go to `Settings/General/Badges` to add a badge.

* **Name**: Coverage
* **Link** (i.e., URL link you want when you click on the badge) : https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/pipelines/%{default_branch}/latest
* **Badge Image URL** : https://gitlab.inria.fr/%{project_path}/badges/%{default_branch}/coverage.svg

*Note*: The badge images uses the coverage value extracted from your regular expression defined with `coverage` keyword in `.gitlab-ci.yml`.

The badge is now visible on your Gitlab project home (for example, see on top of this project page).
